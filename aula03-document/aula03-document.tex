\documentclass{beamer}
\usepackage[brazil]{babel}
\usepackage[T1]{fontenc} %caracteres acentuados
\usepackage[utf8]{inputenc}
\usepackage{color,graphicx}
\usepackage{multirow}
\usepackage{multicol}
%\usepackage{caption} % <- no início do ficheiro !!! Vou comentar porque não funciona no beamer http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=388267
\usepackage{listings}
\usepackage{multimedia}
\usepackage{copyrightbox}

\lstset{
	numbers=left,
	stepnumber=5,
	firstnumber=1,
	numberstyle=\tiny,
	breaklines=true,
	frame=topline,
	basicstyle=\tiny,
	stringstyle=\ttfamily,
	showstringspaces=false,
	extendedchars=\true,
	inputencoding=utf8,
	tabsize=2,
	captionpos=top
}

% Beamer numbered captions
\setbeamertemplate{caption}[numbered]
\numberwithin{figure}{section}
\numberwithin{table}{section}

%\usetheme{CambridgeUS}
\mode<presentation>{\usetheme{AnnArbor}}
\definecolor{uofsgreen}{rgb}{.125,.5,.25}
%\usecolortheme[named=uofsgreen]{structure}
\setbeamertemplate{footline}[page number]
\AtBeginSection[]
{
\begin{frame}
\tableofcontents[currentsection]
\end{frame}
}

\AtBeginSubsection[]
{
\begin{frame}
\tableofcontents[currentsection,currentsubsection]
\end{frame}
}

\title{\textbf{Bancos de dados orientados a documento}}
\author{Eduardo Ferreira dos Santos}
\institute{Ciência da Computação \\ 
Centro Universitário de Brasília -- UniCEUB}

\date{\today} % Date, can be changed to a custom date
\logo{\includegraphics[scale=0.2]{../imagens/logo-ceub}}

\begin{document}
\begin{frame}
\titlepage
\end{frame}

\begin{frame}
    \frametitle{Objetivos da aula}
    \begin{itemize}
        \item Apresentar as principais \alert{características} dos bancos orientados a documento;
        \item Identificar os principais \alert{casos de uso} que eles foram construídos para suportar;
        \item Entender suas \alert{limitações};
        \item Introduzir o MongoDB, banco de dados orientado a documentos que será explorado no curso.
    \end{itemize}
\end{frame}


\begin{frame}
	\frame{Sumário}
	\tableofcontents
\end{frame}

\section{Introdução}

\begin{frame}
    \frametitle{Documentos}
    \begin{itemize}
        \item Bancos de dados desenhadas para armazenar e recuperar documentos \cite{carnegie};
        \item Esquema de armazenamento flexível para dados semi estruturados;
        \item Extensões especializadas de armazenamento chave-valor;
        \item Desenhados com foco em escalabilidade.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Comparativo com chave-valor}
    \begin{itemize}
        \item Desenhados para fornecer a capacidade de examinar o \alert{valor} dos elementos armazenados; 
        \item Do ponto de vista do banco de dados chave-valor, os documentos não são estruturados;
        \item Os bancos orientados a documentos extraem \alert{metadados} para oferecer funcionalidade adicional.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{O que é um documento?}
    \begin{itemize}
        \item Uma coleção de informações \cite{carnegie};
        \item Estrutura de dados auto-descritiva e hierárquica, que contém valores escalares, coleções e dicionários;
        \item Análogo à linha no banco de dados relacional: unidade de armazenamento básica;
        \item Os documentos normalmente são descritos utilizando formatos como \alert{XML e JSON};
        \item Importância dos \alert{metadados}.
    \end{itemize}
\end{frame}


\begin{frame}
    \frametitle{Benefícios dos dados semi-estruturados}
    \begin{itemize}
        \item Qual o significado?
        \item Como você interpreta?
        \item Como poderia ser interpretado por um computador?
    \end{itemize}
    \begin{figure}[ht]
        \centering
        \includegraphics[width=.4\textwidth]{../imagens/address}
        \label{fig:address}
        \caption{Exemplo de endereço \cite{carnegie}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Mais sobre dados semi-estruturados}
    \begin{itemize}
        \item E se mudarmos o formato?
        \item É possível obter mais informações sobre o dado?
        \item E o computador? É capaz de interpretar com mais facilidade?
    \end{itemize}
    \begin{figure}[ht]
        \centering
        \includegraphics[width=.6\textwidth]{../imagens/address-xml}
        \label{fig:address-xml}
        \caption{Endereço no formato XML \cite{carnegie}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Formato JSON}
    \begin{itemize}
        \item Na Web, os dados trafegam no formato JSON;
        \item Algumas extensões mais modernas para tipos de dados aumentam a expressividade;
        \item É mais natural para leitura por computadores.
    \end{itemize}
    \lstinputlisting[language=Python, label=address.json, caption={Exemplo de endereço no formato JSON}]{../codigos/address.json}
\end{frame}

\begin{frame}
    \frametitle{Modelo agregado vs. modelo relacional}
    \begin{itemize}
        \item Dados agregados fornecem suporte a estruturas de dados que são mais próximas das estruturas de um \alert{objeto} nas aplicações;
        \item Objetivo da orientação a agregação \cite{carnegie}: armazenar os dados \alert{no mesmo formato} que deseja recuperá-los; 
        \begin{quote}
            ``Os padrões de acesso de dados das aplicações devem ser prioritários no desenho do esquema'' -- RDBMS to MongoDB Migration Guide, MongoDB Inc., 2015
        \end{quote}
        \item Dados agregados são a unidade única de distribuição em um banco de dados.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Aplicações}
    \begin{itemize}
        \item \emph{Content Management Systems};
        \item Plataformas de blog;
        \item \emph{Web Analytics};
        \item e-commerce;
        \item Registro de eventos (\emph{event logging});
        \item Sistemas onde o tipo de dados armazenado pode mudar dinamicamente.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Quando não utilizar}
    \begin{itemize}
        \item Quando for necessário suportar \alert{transações complexas} que operam em múltiplos documentos;
        \item Quando for necessário construir \alert{relacionamentos complexos} no modelo de dados;
        \item Quando o modelo de dados for bastante conhecido e não houver expectativa de mudança.
    \end{itemize}
\end{frame}


\begin{frame}
    \frametitle{Popularidade}
    \begin{figure}[ht]
        \centering
        \copyrightbox[b]{\includegraphics[width=.9\textwidth]{../imagens/ranking-document}}{Fonte: \url{https://db-engines.com/en/ranking_trend/document+store}}
        \label{fig:ranking-document}
        \caption{Ranking de bancos de dados orientados a documento mais utilizados no db-engines}
    \end{figure}
\end{frame}

\section{MongoDB}


\begin{frame}
    \frametitle{O que é o MongoDB}
    \begin{itemize}
        \item Banco de dados orientado a documentos;
        \item Líder do ranking no db-engines (há muito tempo);
        \item Foco na facilidade de uso e escalabilidade;
        \item Suporte a inúmeras linguagens de programação;
        \item Desenvolvimento pela 10gen (agora MongoDB Inc.) em 2007.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Características principais do MongoDB}
    \begin{itemize}
        \item Consultas ad-hoc;
        \item Indexação primária e secundária;
        \item Replicação;
        \item Divisão (\emph{sharding}) para balanceamento de carga;
        \item Agregação de dados.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Quem utiliza MongoDB}
    \begin{center}
        Praticamente todas as organizações utilizam MongoDB em alguma escala. Difícil imaginar alguma que não use.
    \end{center}
    \begin{figure}[ht]
        \centering
        \copyrightbox[b]{\includegraphics[width=.9\textwidth]{../imagens/mongodb-usage}}{Fonte: \url{https://www.mongodb.com/who-uses-mongodb}}
        \label{fig:mongodb-usage}
        \caption{Ranking de bancos de dados orientados a documento mais utilizados no db-engines}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Modelo de dados no MongoDB -- documentos}
    \begin{itemize}
        \item Os registros no banco de dados são armazenados como documentos;
        \item Os documentos são coleções no formato JSON de chave-valor;
        \item No MongoDB as chaves são denominadas \alert{campos}.
    \end{itemize}
    \begin{figure}[ht]
        \centering
        \includegraphics[width=.8\textwidth]{../imagens/mongodb-document}
        \label{fig:mongodb-document}
        \caption{Exemplo de documento no MongoDB \cite{carnegie}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Modelo de dados no MongoDB -- coleções}
    \begin{itemize}
        \item Os documentos relacionados são armazenados em \alert{coleções} \cite{carnegie};
        \item Podem ser comparadas às tabelas nos bancos de dados relacionais\footnote{Confundir coleções e tabelas pode supor relacionamento, o que \alert{não é o objetivo do banco orientado a documentos}};
        \item Numa coleção, os documentos podem possuir diferentes campos.
    \end{itemize}
    \begin{figure}[ht]
        \centering
        \includegraphics[width=.8\textwidth]{../imagens/mongodb-collection}
        \label{fig:mongodb-collection}
        \caption{Exemplo de coleção no MongoDB \cite{carnegie}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Consultas no MongoDB}
    \begin{itemize}
        \item Mesma sintaxe do JavaScript;
        \item Utilização do \texttt{mongo shell};
        \item \texttt{db.collection.find()}: parecido com o SELECT
        \item Dois parâmetros opcionais:
        \begin{description}
            \item[Consulta] Retorna os documentos que satisfazem as condições de busca (similar a FROM e WHERE);
            \item[Projeção] Retorna os campos solicitados pelo consulta (SELECT \textbf{field1}, \textbf{field2}, ...).
        \end{description}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Consultas no MongoDB - exemplo}
    \begin{itemize}
        \item Ambas as consultas e projeções funcionam em documentos;
        \item \texttt{db.authors.find( {``name.first'': ``Dan''} )}
        \includegraphics[width=.8\textwidth]{../imagens/mongodb-query01}
        \item \texttt{db.authors.find( {``name.first'':``Dan''}, {``name'': 1, \_id:0} )}
        \includegraphics[width=.5\textwidth]{../imagens/mongodb-query02}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Comparativo relacional}
    \begin{itemize}
        \item Algumas definições do banco relacional podem ser comparadas a elementos do MongoDB;
        \item Mesmo apresentando diferença em sentido, existe relação no objetivo;
        \item Importante observar a diferença entre \alert{registro} e \alert{documento};
        \item Todo documento é \alert{auto-contido}.
    \end{itemize}
    \begin{table}
        \begin{tabular}{|c|c|}
            \hline
            \textbf{Relacional} & \textbf{MongoDB} \\
            \hline
            \hline
            Esquema & Banco de dados \\
            \hline 
            Tabela & Coleção \\
            \hline
            Linha & Documento \\
            \hline
            Coluna & Campo \\
            \hline
        \end{tabular}
        \label{tab:mongodb-comparison}
        \caption{Comparação de definições entre MongoDB e relacional}
    \end{table}
\end{frame}

\begin{frame}
    \frametitle{Replicação no MongoDB}
    \begin{itemize}
        \item Replicação do tipo \emph{Master-slave} utilizando conjuntos de réplicas: grupos de instâncias do MongoDB que hospedam o mesmo conjunto de dados;
        \item Primário (\emph{Master}): 
        \begin{itemize}
            \item aceita todas as operações de escrita;
            \item pode garantir consistência para todas as operações de leitura.
        \end{itemize}
        \item Secundário (\emph{Slave}):
        \begin{itemize}
            \item pode ser configurado para aceitar leituras;
            \item recebe as mudanças enviadas pelo \emph{master}.
        \end{itemize}
        \item Se o nó primário perde a comunicação com o conjunto de réplicas, há uma tentativa de selecionar um novo \emph{master} (algoritmo de consenso).
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Fragmentação (\emph{sharding}) no MongoDB}
    \begin{itemize}
        \item Os fragmentos armazenam os dados;
        \item Roteadores de consulta tratam as requisições de leitura/escrita;
        \item Servidores de configuração mapeiam o conjunto de dados aos fragmentos específicos;
        \item Fragmentação acontece no nível da coleção;
        \item Ambas as divisões baseadas em hash e em intervalo de valores estão disponíveis.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Suporte a transações no MongoDB}
    \begin{itemize}
        \item Transações acontecem no nível do documento (atomicidade em nível de documento);
        \item Não há suporte a transações que envolvem mais de um documento;
        \item Transações em múltiplos documentos para conjuntos de réplica estão no roadmap da aplicação.
    \end{itemize}
\end{frame}




\begin{frame}
	\begin{center}
		\textbf{OBRIGADO!!!} \\
		\textbf{PERGUNTAS???}
	\end{center}
\end{frame}

\bibliography{../nosql}
\bibliographystyle{apalike}

\end{document}
