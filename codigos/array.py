exampleAssociativeArray['Pi'] = 3.1415
exampleAssociativeArray['CapitalFrance'] = 'Paris'
exampleAssociativeArray['ToDoList'] = { 
    'Alice' : 'run reports; meeting with Bob', 
    'Bob' : 'order inventory; meeting with Alice' 
}
exampleAssociativeArray[17234] = 34468
