\documentclass{beamer}
\usepackage[brazil]{babel}
\usepackage[T1]{fontenc} %caracteres acentuados
\usepackage[utf8]{inputenc}
\usepackage{color,graphicx}
\usepackage{multirow}
\usepackage{multicol}
%\usepackage{caption} % <- no início do ficheiro !!! Vou comentar porque não funciona no beamer http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=388267
\usepackage{listings}
\usepackage{multimedia}
\usepackage{copyrightbox}

\lstset{
	numbers=left,
	stepnumber=5,
	firstnumber=1,
	numberstyle=\tiny,
	breaklines=true,
	frame=topline,
	basicstyle=\tiny,
	stringstyle=\ttfamily,
	showstringspaces=false,
	extendedchars=\true,
	inputencoding=utf8,
	tabsize=2,
	captionpos=top
}

% Beamer numbered captions
\setbeamertemplate{caption}[numbered]
\numberwithin{figure}{section}
\numberwithin{table}{section}

%\usetheme{CambridgeUS}
\mode<presentation>{\usetheme{AnnArbor}}
\definecolor{uofsgreen}{rgb}{.125,.5,.25}
%\usecolortheme[named=uofsgreen]{structure}
\setbeamertemplate{footline}[page number]
\AtBeginSection[]
{
\begin{frame}
\tableofcontents[currentsection]
\end{frame}
}

\AtBeginSubsection[]
{
\begin{frame}
\tableofcontents[currentsection,currentsubsection]
\end{frame}
}

\title{\textbf{Bancos de dados de família de colunas}}
\author{Eduardo Ferreira dos Santos}
\institute{Ciência da Computação \\ 
Centro Universitário de Brasília -- UniCEUB}

\date{\today} % Date, can be changed to a custom date
\logo{\includegraphics[scale=0.2]{../imagens/logo-ceub}}

\begin{document}
\begin{frame}
\titlepage
\end{frame}

\begin{frame}
    \frametitle{Objetivos da aula}
    \begin{itemize}
        \item Apresentar as principais \alert{características} dos bancos de dados de família de colunas;
        \item Identificar os principais \alert{casos de uso} que eles foram construídos para suportar;
        \item Entender suas \alert{limitações};
        \item Introduzir o Cassandra, banco de dados de família de colunas que será explorado no curso.
    \end{itemize}
\end{frame}


\begin{frame}
	\frame{Sumário}
	\tableofcontents
\end{frame}

\section{Introdução}

\begin{frame}
    \frametitle{Definições}
    \begin{itemize}
        \item A \alert{coluna} é uma unidade de armazenamento básica no banco de dados de família de colunas \cite{sullivan2015nosql};
        \item Uma coluna é definida como:
        \begin{itemize}
            \item Nome;
            \item Valor.
        \end{itemize}
        \item Alguns bancos de dados armazenam um \emph{timestamp} juntamente com o nome e o valor;
        \item Um conjunto de colunas formam uma \alert{linha};
        \item As linhas podem possuir as mesmas colunas ou colunas diferentes;
        \item Colunas também podem possuir outras colunas. Nesse caso são chamadas \alert{super colunas}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Exemplos de colunas}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.8\textwidth]{../imagens/row-columns}
		\label{fig:row-columns}
		\caption{Uma linha consiste em uma ou mais colunas. Linhas diferentes podem ter colunas diferentes \cite{sullivan2015nosql}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Famílias de colunas}
    \begin{itemize}
        \item Quando existem muitas colunas, pode ajudar agrupá-las em coleções de colunas relacionadas;
        \item No exemplo da Figura \ref{fig:row-columns}, \texttt{FirstName} e \texttt{LastName} são usados juntos frequentemente, então poderiam ser agrupadas;
        \item O mesmo raciocínio vale para \texttt{OfficeNumber} e \texttt{OfficePhoneNumber}, por exemplo;
        \item O agrupamento em coleções de colunas que são utilizadas juntas são chamadas de \alert{famílias de colunas} \cite{sullivan2015nosql}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Schema}
    \begin{itemize}
        \item Assim como os bancos de dados orientados à documento, os bancos de dados de famílias de colunas não precisam de um schema pré-definido;
        \item Os desenvolvedores podem adicionar novos dados conforme a necessidade;
        \item Cada linha pode ter diferentes conjuntos de colunas e super colunas;
        \item Não é incomum que os bancos de dados de famílias de colunas forneçam suporte a milhões de colunas.
    \end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Características}
	\begin{itemize}
		\item Os dados são armazenados em famílias de colunas como linhas que possuem muitas colunas associadas a uma chave;
		\item Famílias de colunas são grupos de dados relacionados que normalmente são acessados juntos;
		\item Ex.: para um \texttt{Cliente} é provável que o \texttt{Perfil} seja acessado ao mesmo tempo, mas nem sempre seus \texttt{Pedidos}.
	\end{itemize}
	\begin{figure}[ht]
		\centering
		\includegraphics[width=.65\textwidth]{../imagens/cassandra-model}
		\label{fig:cassandra-model}
		\caption{Modelo de dados do Cassandra com famílias de colunas \cite{nosql-distiled}}
	\end{figure}
\end{frame}


\begin{frame}
    \frametitle{Popularidade}
    \begin{figure}[ht]
        \centering
        \copyrightbox[b]{\includegraphics[width=.9\textwidth]{../imagens/cassandra-usage}}{Fonte: \url{https://db-engines.com/en/ranking_trend/wide+column+store}}
        \label{fig:cassandra-usage}
        \caption{Ranking de bancos de dados de famílias de coluna mais utilizados no db-engines}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Nomenclatura}
    \begin{itemize}
        \item A comunidade de bancos de dados possui definições claras em relação aos bancos de dados em grafo, chave-valor e orientados à documentos;
        \item O mesmo não acontece para bancos de dados de famílias de colunas:
        \begin{itemize}
            \item São chamados de \emph{wide columns databases} (bancos de dados de muitas colunas) devido à capacidade de gerenciar dezenas de milhares de colunas (ou mais);
            \item Também são chamadas de \emph{data stores} ao invés de \emph{databases} porque não possuem algumas funcionalidades dos bancos de dados relacionais.
        \end{itemize}
        \item A utilização do termo \alert{família de colunas} serve a dois propósitos \cite{sullivan2015nosql}:
        \begin{enumerate}
            \item Enfatizar a importância do agrupamento de colunas relacionadas;
            \item Reforçar a ideia de que bancos de dados NoSQL são SGBDs reais, mesmo sem possuir todas as funcionalidades de bancos de dados relacionais.
        \end{enumerate}
        \item Os bancos de dados relacionais são muito importantes, mas não definem o que pode ser chamado de Sistema Gerenciador de Bancos de Dados.
    \end{itemize}
\end{frame}

\section{Definições}

\begin{frame}
    \frametitle{Google BigTable}
    \begin{itemize}
        \item No começo, havia o Google BigTable, com as seguintes funcionalidades \cite{sullivan2015nosql}:
        \begin{itemize}
            \item Os desenvolvedores possuem controle dinâmico sobre as colunas;
            \item Os valores dos dados são indexados pelo identificador da linha, nome da coluna e \emph{timestamp};
            \item Tanto DBAs quando programadores possuem controle sobre a localização física dos dados;
            \item As operações de leitura e escrita em uma linha são atômicas;
            \item As linhas são mantidas em um formato ordenado.
        \end{itemize}
        \item O BigTable oferecia o meio do caminho em relação à definição de estruturas de dados:
        \begin{itemize}
            \item O DBA pode definir as famílias de colunas;
            \item O desenvolvedor pode adicionar colunas a uma família dinamicamente;
            \item Não existe necessidade de \alert{atualização de schema}.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Exemplo de armazenamento}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.7\textwidth]{../imagens/column-family01}
		\label{fig:column-family01}
		\caption{Uma linha em um banco de dados de família de colunas é organizada como um conjunto de famílias de colunas. Cada família é um conjunto de colunas relacionadas; os valores são indexados por linha, nome da coluna e \emph{timestamp} \cite{sullivan2015nosql}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Schema dinâmico e indexação}
    \begin{itemize}
        \item A utilização de famílias de colunas permitem a utilização de controle dinâmico sobre as colunas, integrando DBAs e programadores;
        \item Indexação por linha, nome da coluna e \emph{timestamp}:
    \end{itemize}
    \begin{minipage}[ht]{0.54\textwidth}
	    \begin{itemize}
	        \item Características:
	        \begin{itemize}
	            \item O identificador da linha tem a mesma função da chave primária em um banco de dados relacional;
	            \item Diferente dos bancos de dados relacionais que são orientados às linhas, os bancos de dados de famílias de colunas armazenam somente pedaços das linhas juntas;
	            \item Podem existir múltiplas versões  das colunas, com \emph{timestamps} diferentes;
	            \item O mais recente é sempre considerado.
	        \end{itemize}
	    \end{itemize}    
    \end{minipage}
    \begin{minipage}[ht]{0.44\textwidth}
	    \begin{figure}[ht]
	        \centering
			\includegraphics[width=.9\textwidth]{../imagens/column-family02}
			\label{fig:column-family02}
			\caption{Exemplo de indexação \cite{sullivan2015nosql}}
		\end{figure}    
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Localização física}
    \begin{itemize}
        \item Um dos maiores gargalos em bancos de dados é recuperar os dados do \alert{disco};
        \item Uma consulta num banco de dados relacional pode requerer informações que estão armazenadas fisicamente em diferentes partes do disco;
        \item Os bancos de dados de famílias de colunas armazenam as colunas de maneira contígua;
        \item O formato de armazenamento aumenta a possibilidade de recuperar todos os dados armazenados em uma única rotação do disco.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Problemas de armazenamento}
    Em algum caso pode ser uma má ideia armazenar os dados juntos. Como agrupar as vendas por estado no caso da Figura \ref{fig:column-storage-disk}?
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.5\textwidth]{../imagens/column-storage-disk}
		\label{fig:column-storage-disk}
		\caption{Exemplo de armazenamento em coluna e famílias \cite{sullivan2015nosql}}
	\end{figure}    
\end{frame}

\begin{frame}
    \frametitle{Leitura e escrita atômicas}
    \begin{itemize}
        \item No BigTable todas as operações de leitura e escrita são atômicas, independente do número de colunas;
        \item Cada operação de leitura recupera todas as colunas ou nenhuma delas;
        \item Não existem \alert{resultados parciais};
        \item O mesmo vale para as operações de atualização: ou todas são bem sucedidas ou falham;
        \item Ao atualizar diferentes colunas, ou todas são atualizadas ou a operação falha.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Operações atômicas}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/column-atomic}
		\label{fig:column-atomic}
		\caption{Todas as operações são atômicas \cite{sullivan2015nosql}}
	\end{figure} 
\end{frame}

\begin{frame}
    \frametitle{Ordenação das linhas}
    \begin{itemize}
        \item No BigTable, todas as linhas são mantidas de forma ordenada;
        \item Facilita a organização de consultas por intervalo. Ex.: vendas no mês;
        \item Por serem mantidas ordenadas, não é necessário ordenar uma tabela muito grande ou utilizar um índice secundário para manter a ordenação;
        \item Importância de definir a \alert{ordem das colunas}, pois só é possível ordenar em um sentido (ascendente ou descendente).
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Bancos de dados de famílias de colunas}
    \begin{itemize}
        \item Os bancos de dados chave-valor são os mais simples na arquitetura NoSQL \cite{sullivan2015nosql}:
        \begin{itemize}
            \item Possuem uma estrutura lógica para isolar as chaves (\emph{keyspace});
            \item Associam um valor a cada chave;
            \item Permitem um \emph{keyspace} para cada aplicação ou um único para todas as aplicações.
        \end{itemize}
        \item As famílias de colunas são análogas aos \emph{keyspaces} dos bancos de dados chave-valor;
        \item Os desenvolvedores podem adicionar novas colunas às famílias de colunas livremente;
        \item Na terminologia do Cassandra, um \emph{keyspace} é análogo a um banco de dados no banco de dados relacional;
        \item Diferente dos bancos de dados chave-valor, os valores nas colunas são indexados por um identificador de linha, assim como o nome de coluna.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Analogia ao chave-valor}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.7\textwidth]{../imagens/column-keyspace}
		\label{fig:column-keyspace}
		\caption{Comparativo entre \emph{keyspaces} nos bancos chave-valor e famílias de colunas \cite{sullivan2015nosql}}
	\end{figure}   
\end{frame}

\begin{frame}
    \frametitle{Analogia ao relacional}
    \begin{itemize}
        \item Tanto relacional como famílias de colunas utilizam uma \alert{chave};
        \item Em famílias de colunas se chamam \alert{chave de linha} (\emph{row key});
        \item Ambos podem ser considerados ideais para armazenamento de dados tabulados, com alguma abstração;
        \item Os bancos de famílias de colunas utilizam o conceito de \alert{mapeamento} (\emph{maps});
        \begin{itemize}
            \item Também conhecidos como dicionários ou \emph{arrays} associativos;
            \item Uma chave de coluna mapeia um nome de coluna para seu valor associado;
            \item Uma família de coluna é um mapeamento / dicionário / \emph{array} associativo que aponta para um mapeamento / dicionário / \emph{array} associativo de colunas;
        \end{itemize}
        \item Em famílias de colunas não existe o conceito de \alert{tipo} para a coluna;
        \item Os dados dos colunas são valores binários interpretados pela aplicação, não pelo banco;
        \item A validação deve acontecer \alert{antes da inserção} no banco de dados.
    \end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Comparativo de terminologias}
	\begin{itemize}
		\item Apesar de similaridades superficiais com os bancos relacionais, existem importantes diferenças;
		\item Ainda assim, existem similaridades, como linguagem de consulta similar ao SQL e alguns nomes (linhas e colunas).
	\end{itemize}
	\begin{table}[ht]
		\centering
		\includegraphics[width=.8\textwidth]{../imagens/column-rdbms}
		\label{fig:column-rdbms}
		\caption{Tabela comparativa de características de bancos de família de coluna e RDBMS \cite{nosql-distiled}}
	\end{table}
\end{frame}


\section{Cassandra}


\begin{frame}
	\frametitle{Cassandra}
	\begin{itemize}
		\item Cassandra é um dos bancos de dados orientados a famílias de colunas mais utilizados no db-engines;
		\item Existem outros, como DynamoDB, HBase, Hypertable;
		\item Rápido e facilmente escalável;
		\item As operações de escrita são propagadas pelo Cluster;
		\item O cluster não tem um nó mestre, então qualquer operação de escrita ou leitura pode ser executada por qualquer nó;
		\item Eventualmente consistente.
	\end{itemize}
\end{frame}

\subsection{Características}

\begin{frame}
	\frametitle{Características do Cassandra}
	\begin{itemize}
		\item A unidade de armazenamento básica é uma coluna;
		\item Cada coluna consiste num par chave-valor onde o nome também se comporta como chave;
		\item Cada um dos pares de chave-valor é uma coluna única armazenada com a informação de \texttt{timestamp};
		\item O \texttt{timestamp} é utilizado para:
		\begin{itemize}
			\item  expirar dados;
			\item resolver conflitos de escrita;
			\item lidar com dados desatualizados;
			\item etc.
		\end{itemize}
		\item O \texttt{timestamp} é um componente central para a sincroniza do banco.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Exemplo de dado}
	\begin{itemize}
		\item A \alert{coluna} possui como chave \texttt{fullName} e como valor \texttt{Martin Fowler};
		\item Cada \alert{coluna} possui um \texttt{timestamp} anexado
	\end{itemize}
	\lstinputlisting[language=Python, label=cassandra01.cql, caption={Exemplo de coluna armazenada no Cassandra}]{../codigos/cassandra01.cql}
\end{frame}


\begin{frame}
	\frametitle{Exemplo de famílias de colunas}
	\begin{itemize}
		\item Agora observemos o exemplo onde a coluna tem como chave \texttt{firstName} e valor \texttt{Martin};
		\item Uma linha é uma coleção de colunas conectadas por uma chave;
		\item Uma coleção de linhas similares constitui uma \alert{família de colunas};
		\item Quando as colunas em uma família são simples recebem o nome de \alert{família de colunas padrão};
	\end{itemize}
	\lstinputlisting[language=Python, label=cassandra02.cql, caption={Exemplo de família de colunas padrão no Cassandra}, captionpos=top, numbers=left, stepnumber=1]{../codigos/cassandra02.cql}
\end{frame}

\begin{frame}
	\frametitle{Comparação com RDBMS}
	\begin{itemize}
		\item Cada família de colunas pode ser comparada a um conjunto de linhas numa tabela relacional;
		\item A chave identifica a linha, e cada linha consiste em múltiplas colunas;
		\item A principal diferença é que as múltiplas linhas \alert{não possuem as mesmas colunas};
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Super colunas}
	\begin{itemize}
		\item Quando uma coluna contém um mapeamento de outras colunas, então se formam as \alert{super colunas};
		\item Uma super coluna é formada com um nome e um valor, que também é um conjunto de colunas;
		\item Pode ser definida como um conjunto de colunas.
	\end{itemize}
	\lstinputlisting[language=Python, label=cassandra03.cql, caption={Exemplo de super colunas no Cassandra}, captionpos=top, numbers=left, stepnumber=1]{../codigos/cassandra03.cql}
\end{frame}

\begin{frame}
	\frametitle{Super famílias de colunas}
	\begin{center}
		Quando super colunas são utilizadas para criar famílias de colunas, temos uma \alert{super família de colunas}.
	\end{center}
	\begin{minipage}[ht]{0.49\textwidth}
		\lstinputlisting[language=Python, label=cassandra04.cql,  basicstyle=\tiny, numbers=none]{../codigos/cassandra04-1.cql}	
	\end{minipage}
	\begin{minipage}[ht]{0.49\textwidth}
		\lstinputlisting[language=Python, label=cassandra04.cql, basicstyle=\tiny, numbers=none]{../codigos/cassandra04-2.cql}	
	\end{minipage}
\end{frame}

\begin{frame}
	\frametitle{Armazenamento e recuperação}
	\begin{itemize}
		\item As super famílias de colunas são utilizadas para manter juntos os dados relacionados;
		\item Todas as colunas são sempre recuperadas e processadas pelo Cassandra;
		\item Se algumas colunas não forem sempre necessárias pode resultar em processamento desnecessário;
		\item Tanto famílias quanto super famílias são armazenadas no \alert{keyspace};
		\item Abordagem similar à tabela no modelo relacional, onde todas as famílias de colunas relacionadas à aplicação são armazenadas;
		\item Os \emph{keyspaces} precisam ser criados para que famílias de colunas possam ser atribuídas a eles.
	\end{itemize}
	\lstinputlisting[language=Python, label=cassandra05.cql, caption={Exemplo de criação de keyspace}]{../codigos/cassandra05.cql}
\end{frame}

\begin{frame}
	\frametitle{Operação de escrita}
	\begin{itemize}
		\item Os dados são escritos primeiro em um registro de gravações e posteriormente escritos numa estrutura em memória conhecida como \alert{memtable};
		\item A operação de escrita é considerada bem sucedida de armazenada em ambos (registro e \emph{memtable});
		\item As gravações são armazenadas em memória e escritas periodicamente em estruturas conhecidas como \alert{SSTable};
		\item Após consolidadas, as \emph{SSTables} são limpas (\emph{flush}) e novas são geradas por novas operações de escrita;
		\item \emph{SSTables} não utilizadas precisam ser limpas por uma operação de compactação.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Consistência}
	\begin{itemize}
		\item Para garantir a consistência das informações o Cassandra utiliza um ajuste numérico;
		\item Quando a consistência é \texttt{ONE} para as operações de leitura, os dados da \alert{primeira réplica} encontrada são retornados, ainda que existam alterações pendentes;
		\item Se o dado estiver desatualizado, as operações de leitura subsequentes recebem os dados mais atualizados: processo de \alert{read repair};
		\item Níveis \alert{mais baixos} de consistência são interessantes quando \alert{não há} necessidade de obter \alert{sempre} os dados \alert{mais recentes};
		\item Também é interessante quando a \alert{performance} é um requisito;
		\item De maneira similar, com baixo nível de consistência o Cassandra deve retornar uma resposta ao \alert{escrever no registro de transações} do \alert{primeiro nó conectado};
		\item Existe risco de \alert{perda de dados}.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Aumentando a consistência}
	\begin{itemize}
		\item Na consistência ajustada para \texttt{QUORUM}  a \alert{maior parte} dos nós deve responder à operação;
		\item Se for leitura deve ser retornado o registro \alert{mais recente} encontrado;
		\item Se for escrita o dado deve ser \alert{propagado para a maior parte} dos nós;
		\item Quando a consistência está ajustada para \texttt{ALL} \alert{todos} os nós devem responder;
		\item A falha de \alert{um dos nós} em responder implica na \alert{falha} da operação.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Transações}
	\begin{itemize}
		\item O Cassandra não possui suporte a transações da forma tradicional: iniciar múltiplas operações de escrita e decidir se as mudanças devem ser \alert{consolidadas};
		\item A operação de escrita é \alert{atômica} para a \alert{linha};
		\item Inserir ou remover colunas para uma linha será sempre considerada uma \alert{operação única} de escrita, que deve ser bem sucedida ou falhar;
		\item Escrita no registro de transações e na \emph{memtable};
		\item Se um nó cair, a transação é refeita naquele nó (similar ao \emph{redo log} do Oracle);
		\item Suporte a ferramentas externas de sincronia: \texttt{zookeeper} e \texttt{cages}.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Disponibilidade}
	\begin{itemize}
		\item O Cassandra é construído com foco na \alert{alta disponibilidade};
		\item Não existe nó Master, uma vez que qualquer nó pode receber operações de escrita;
		\item A disponibilidade do cluster é calculada levando em consideração o \alert{nível de consistência}  e o \alert{número de nós};
		\item Os valores de \alert{consistência} para o \alert{keyspace} devem ser ajustados de acordo com as \alert{necessidades do projeto}:
		\begin{itemize}
			\item Alta disponibilidade;
			\item Alta disponibilidade para leitura.
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Consultas}
	\begin{itemize}
		\item O modelo de dados no Cassandra deve ser ajustado para fazer com que as colunas e famílias de colunas sejam otimizadas para leitura;
		\item A linguagem de consulta não possui \emph{features} avançadas;
		\item À medida que os dados são inseridos nas famílias de colunas são ordenados pelo nome das colunas;
		\item Se uma coluna é mais utilizada do que as outras, é uma boa prática utilizá-la como chave.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Escalabilidade}
	\begin{itemize}
		\item Para escalar o Cassandra basta adicionar mais nós ao Cluster;
		\item Como nenhum nó é o Master, adicionar novos nós aumenta a capacidade de leitura e escrita;
		\item Facilidade de escalamento horizontal traz diversas vantagens:
		\begin{itemize}
			\item Tempo de missão máximo (\alert{uptime});
			\item Adição transparente de novos servidores.
		\end{itemize}
		\item Não existe \emph{downtime} ao adicionar novos nós.
	\end{itemize}
\end{frame}

\subsection{Utilização}

\begin{frame}
    \frametitle{Quando utilizar}
    \begin{itemize}
        \item Desenvolvidos para ambientes de \alert{grande volumes de dados} que precisam de:
        \begin{itemize}
            \item Muita performance na escrita;
            \item Um grande número de de servidores;
            \item Distribuição geográfica dos dados.
        \end{itemize}
        \item Se pelo menos um nó estiver funcionando, o Cassandra sempre vai receber operações de escrita;
        \item Redes sociais (muitas escritas simultâneas) são bons candidatos;
        \item O Cassandra é ideal para ambientes distribuídos. Se a necessidade for apenas flexibilidade de schema, existem alternativas mais simples (orientados a documentos e chave-valor);
        \item Foco em \alert{escalabilidade}.
    \end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Caso de uso -- registro de eventos}
	\begin{itemize}
		\item Bancos de dados de famílias de colunas são uma ótima opção para registrar eventos;
		\item Ex.: erros e ou estado das aplicações;
		\item No Cassandra, os exemplos das aplicações podem ser registrados no formato \texttt{appname:timestamp};
		\item Como é possível escalar as operações de escrita, registrar os eventos não deve ser um problema.
	\end{itemize}
	\begin{figure}[ht]
		\centering
		\includegraphics[width=.8\textwidth]{../imagens/event-log}
		\label{fig:event-log}
		\caption{Registro de eventos no Cassandra \cite{nosql-distiled}}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Netflix}
	 \begin{figure}[ht]
        \centering
        \copyrightbox[b]{\includegraphics[width=.9\textwidth]{../imagens/cassandra-netflix}}{Fonte: \url{https://www.slideshare.net/VinayKumarChella/how-netflix-manages-petabyte-scale-apache-cassandra-in-the-cloud}}
        \label{fig:cassandra-netlix}
        \caption{Utilização do Cassandra no Netflix}
    \end{figure}
\end{frame}

\begin{frame}
	\frametitle{Quando não usar}
	\begin{itemize}
		\item Anti-pattern muito claro: necessidade de transações ACID para leitura e escrita;
		\item Ambientes de baixa maturidade técnica;
		\item Ambientes que necessitam de mudança acelerada: alterar o desenho das famílias de colunas é necessário e pode ser lento demais;
		\item Custo das mudanças:
		\begin{description}
			\item[RDBMS]  Custo elevado para alterar o esquema; custo baixo para alterar as consultas.
			\item[Cassandra] Custo baixo para alterar o esquema; custo elevado para alterar as consultas.
		\end{description}	
	\end{itemize}
\end{frame}


\begin{frame}
	\begin{center}
		\textbf{OBRIGADO!!!} \\
		\textbf{PERGUNTAS???}
	\end{center}
\end{frame}

\bibliography{../nosql}
\bibliographystyle{apalike}

\end{document}
